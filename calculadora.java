public class Calculadora {
    public static void main(String[] args) {
        int a = 5;
        int b = 3;
        int suma = a + b;
        int resta = a - b;
        int multiplicacio = a * b;
        int divisio = 0;
        if (b != 0) {
            divisio = a / b;
        } else {
            System.out.println("Error: Divisió per 0");
        }
        System.out.println("La suma és: " + suma);
        System.out.println("La resta és: " + resta);
        System.out.println("La multiplicació és: " + multiplicacio);
        System.out.println("La divisió és: " + divisio);
    }
}

